using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public BulletScript bulletPrefab;
    private Transform _transform;


    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameEvents.Instance.InvokePlayerShootEvent();
            var spawnedBullet = Instantiate(bulletPrefab,_transform.position,_transform.rotation);
            Destroy(spawnedBullet.gameObject, 5f);
           
        }
        
    }
}
