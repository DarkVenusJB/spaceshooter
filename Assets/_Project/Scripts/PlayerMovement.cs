using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float MoveSpeed = 5f;
    private Rigidbody2D _rigidbody;

    // Start is called before the first frame update
    void Start()
    {
       _rigidbody = this.GetComponent<Rigidbody2D>(); 
    }

   

    // Update is called once per frame
    void FixedUpdate()
    {
        float vertical = Input.GetAxisRaw("Vertical");
         float horizontal = Input.GetAxisRaw("Horizontal");

         Vector2 moveDirection = new Vector2(horizontal,vertical);
         _rigidbody.velocity = moveDirection.normalized * MoveSpeed;
        
    }
}
