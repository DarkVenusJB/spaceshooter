using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSystem : MonoBehaviour
{
    [SerializeField] private List<AudioClip> playerShootingClips; // Создаём объект в котором будут храниться наши аудиоклипы
    private List<AudioSource> _shootingSounds = new List<AudioSource>(); // Создаём объект, в котором будем хрпанить наши источники звука

    public static SoundSystem Instance; // Создаём экземпляр объекта для использования из других скриптов

    private void  Awake()  // Запускается в начале игры перед Start()
    {
      if (Instance == null) // Если экземпляр объекта равен нулю, присваевается к текущему объекту, в противном сучае, удаляет  текущий компонент
      {
        Instance = this;
      }  
      else
      {
        Destroy(this);
      }
    }

   
    private void Start()
    {
      foreach(var clip in playerShootingClips) // Перебирая все наши клипы , конвертируем их в источники
      {
        _shootingSounds.Add(ConvertClipToComponent(clip));
      }  
    }

    private void  OnEnable() 
    {
      GameEvents.Instance.OnPlayerShoot += PlayRandomShootSound; 
    }

    private void OnDisable() 
    {
      GameEvents.Instance.OnPlayerShoot += PlayRandomShootSound;
    }

    private void PlayRandomShootSound()
    {
      int randomIndex = Random.Range(0, _shootingSounds.Count);
      AudioSource randomSource =  _shootingSounds[randomIndex];
      randomSource.PlayOneShot(randomSource.clip);
    }

    private AudioSource ConvertClipToComponent(AudioClip clipToConvert) // Создаём метод для конвертации клипа в источник звука
    {
        AudioSource shootingSource = gameObject.AddComponent<AudioSource>(); // Добавляем на игровой объект наш источник звука
        shootingSource.clip = clipToConvert; // Выбираем клип для конвертации
        shootingSource.playOnAwake = false;  // звук не будет проигрываться при появении
        shootingSource.volume = 0.05f; // громкость звука
        return shootingSource;  // Возвращаем наш созданный источник
    }

    
    void Update()
    {
        
    }
}
